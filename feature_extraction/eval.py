import pandas as pd
import numpy as np
import scipy
import matplotlib.pyplot as plt


def draw_DET(FR, FA):

    plt.grid(True, which='both')

    plt.xlim([1, 100])
    plt.ylim([1, 100])
    plt.xlabel('FR rate, %')

    plt.ylabel('FA rate, %')
    plt.xscale('log')
    plt.yscale('log')
    plt.plot(FR, FA)


def get_dist_matrix(data, col_names, threshold, metric):

    
    dvec_dim = len(col_names)
    enroll_test_ratio = 0.5

    g = data.groupby('name')

    enroll_dvecs = []
    test_dvecs = []

    labels = []
    exclude_index = []

    # ================== Enroll and Test
    for idx, (name, group) in enumerate(g):
        l = group['label'].iloc[0]
        enroll_size = int(np.floor(group.shape[0] * enroll_test_ratio))

        enroll = group.reset_index().ix[
            :enroll_size - 1, col_names + ['true_class_prob']]
        test = group.reset_index().ix[
            enroll_size:, col_names + ['true_class_prob']]

        enroll_threshed = enroll.ix[enroll.true_class_prob >= threshold, :]
        test_threshed = test.ix[test.true_class_prob >= threshold, :]

        if (enroll_threshed.shape[0] == 0 or test_threshed.shape[0] == 0):
            exclude_index.append(idx)

            t = enroll.drop(['true_class_prob'], axis=1)
            enroll_dvec_cur = t.mean()

            t = test.drop(['true_class_prob'], axis=1)
            test_dvec_cur = t.mean()

        else:
            t = enroll_threshed.drop(['true_class_prob'], axis=1)
            enroll_dvec_cur = t.mean()

            t = test_threshed.drop(['true_class_prob'], axis=1)
            test_dvec_cur = t.mean()

        enroll_dvecs.append(enroll_dvec_cur)
        test_dvecs.append(test_dvec_cur)
        labels.append(l)

    print 'excluded %d  files out of %d' % (len(exclude_index), idx + 1)
    # ====================

    dvec = pd.DataFrame()
    dvec['label'] = labels

    dvec = dvec.join(pd.DataFrame(np.vstack(enroll_dvecs)))
    test_dvec = pd.DataFrame(np.vstack(test_dvecs))

    del enroll_dvecs, test_dvecs

    dvec_m = dvec.join(test_dvec, rsuffix='_')
    dvec_m.sort('label', inplace=True)

    # ================== Compute distance
    enroll_cols = [str(x) for x in range(dvec_dim)]
    test_cols = [str(x) + '_' for x in range(dvec_dim)]

    dist_mat = scipy.spatial.distance.cdist(
        dvec_m.ix[:, enroll_cols], dvec_m.ix[:, test_cols], metric)
    # ==================

    # print  'exclude_index: ', exclude_index

    # NOW SET TO = 2 ROWS AND COLUMNS CORRESPONDING TO ones we exclude
    max_ = float(np.max(dist_mat.ravel()))
    print max_
    for c in exclude_index:
        row_num = np.where(dvec_m.index == c)[0]
        dist_mat[row_num, :] = 2
        dist_mat[:, row_num] = 2
    # ===========

    return dvec_m, dist_mat


def draw_curves(data, metric='euclidean', threshold=-1, plot=True):

    dvec_m, d = get_dist_matrix(data, threshold, metric)

    if plot:
        plt.figure(figsize=(16, 16))
        plt.imshow(d / np.max(d), interpolation='nearest')
    #plt.savefig(savedir + '/dist',format = 'pdf')

    # =============== Get FR FA
    df = pd.DataFrame(d)
    mask = df.copy()
    mask = (mask == False)  # to make it boolean

    for lab in dvec_m.label.drop_duplicates():
        mask.ix[dvec_m.label == lab, dvec_m.label == lab] = True

    allow_golden = mask.sum().sum()
    rejects_golden = (~mask).sum().sum()

    print allow_golden, rejects_golden

    FR = []
    FA = []

    min_ = float(np.min(df.values.ravel()))
    max_ = float(np.max(df.values.ravel()))

    step_ = (max_ - min_) / 200.0
    print min_, max_, step_
    thresholds = np.arange(min_, max_ + step_, step_)


#     f = open(savedir + '/FR_FA.txt','w')
#     f.write('%s, %s, %s\n' % ('alpha','FR','FA'))
    for alpha in thresholds:
        r = ((df > alpha) & mask).sum().sum()
        a = ((df < alpha) & (~mask)).sum().sum()

        FR.append(float(r) / allow_golden * 100)
        FA.append(float(a) / rejects_golden * 100)

#         f.write('%f, %f, %f\n' % (alpha,FR[-1],FA[-1]))
#     f.close()

    # Draw plot
    if plot:

        plt.figure(figsize=(16, 16))

        draw_DET(FR, FA)
        plt.plot([0, 100], [0, 100])
        #plt.savefig(savedir + '/DET',format = 'svg')
        plt.show()
    return FR, FA
