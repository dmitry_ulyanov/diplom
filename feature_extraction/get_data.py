import numpy as np
import pandas as pd
caffe_root = '/home/dmitry/caffe_vis/'
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe
from caffe.proto import caffe_pb2
import hickle


def classify_from_hdf5_two(hdf5_path_list, _deploy, _snapshot, out_layers):

    data = hickle.load(open(hdf5_path_list[0], 'r'))
    data_val = hickle.load(open(hdf5_path_list[1], 'r'))

    net = caffe.Classifier(_deploy, _snapshot)
    net.set_phase_test()
    net.set_mode_gpu()

    print 'Starting'
    print data['data'].shape

    out = net.forward_all(
        blobs=out_layers, **{net.inputs[0]: np.concatenate((data['data'], data_val['data']))})

    print out.keys()

    del net
    return (out, np.concatenate((data['tag'], data_val['tag'])), np.concatenate((data['label'], data_val['label'])))


def classify_from_hdf5(hdf5_path, _deploy, _snapshot, out_layers):

    if isinstance(hdf5_path, list):
        hdf5_path = hdf5_path[0]

    data = hickle.load(open(hdf5_path, 'r'))

    net = caffe.Classifier(_deploy, _snapshot)
    net.set_phase_test()
    net.set_mode_gpu()

    print 'Starting'
    print data['data'].shape

    out = net.forward_all(blobs=out_layers, **{net.inputs[0]: data['data']})

    print out.keys()

    del net
    return (out, data['tag'], data['label'])


def get_df(hdf5_path, _deploy, _snapshot, out_layers):
    (out, tags, label) = classify_from_hdf5(
        hdf5_path, _deploy, _snapshot, out_layers)
    data = pd.DataFrame()
    data['label'] = np.array(label)
    data['name'] = np.array([x[:-6] for x in tags])
    data['n'] = np.array([x[-5:] for x in tags])

    for out_layer in out_layers:
        num_output = out[out_layer].shape[1]
        data = data.join(pd.DataFrame(np.vstack(np.squeeze(out[out_layer])), columns=[
            out_layer + '_' + str(x) for x in range(num_output)]))

    del out, tags, label
    return data
