
# coding: utf-8

# ## Functions

# In[1]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')

caffe_root = '/home/dmitry/caffe_vis/'  
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe
from caffe.proto import caffe_pb2

get_ipython().magic(u'run feature_extraction/get_data.py')
get_ipython().magic(u'run feature_extraction/eval.py')

def llfun(pred):
    
    epsilon = 1e-15
    pred = np.maximum(epsilon, pred)
    pred = np.minimum(1-epsilon, pred)
    ll = np.sum(np.log(pred))
    ll = ll * -1.0/len(pred)
    return ll


# ## Load data

# In[4]:

import pandas as pd
data = pd.read_csv('data')


# ### or

# In[2]:

data_path = '../data/TIMIT/timit_id/db/sa'
exec_dir = '../data/execs/sa_val_data'

models_to_use  = []
what = 'val'
name = 'bug_less2990'
m = {
'name' : name,
'legend' : 'model 1, 40k',
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_340000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
models_to_use.append(m)

res = []
for m in models_to_use:
    data = get_df(m['test_path'],m['deploy'],m['snapshot'],['ip1','prob']) 
   
    
    data['true_class_prob'] = -1
    unique_labs = data.label.drop_duplicates() 
    for l in unique_labs:
        mask = data.label == l
        data.ix[mask, 'true_class_prob'] = data.ix[mask, 'prob_'+str(l)]
      
    data.sort (['name','n'],inplace = True)
    #res.append(draw_curves(data,0.5,True))

col_names = ['ip1_' + str(x) for x in range(200)]


# ## Compute FR, FA

# In[28]:

get_ipython().magic(u'run feature_extraction/get_data.py')
get_ipython().magic(u'run feature_extraction/eval.py')
import hickle
th_range = np.arange(0.0,0.3,0.05)
for th in th_range:
    print th
    (FR,FA) = draw_curves(data, col_names, 'cosine',th,False)
    hickle.dump((FR,FA),'../data/th/'+str(th),'w')


# ## Plot it

# In[30]:

import hickle
plt.figure(figsize=(16,16))
for th in th_range:
    (FR,FA) = hickle.load('../data/th/'+str(th),safe = False )
    draw_DET(FR,FA)


plt.legend(th_range.astype(np.str))
plt.show()


# ## Frame accuracy

# In[20]:

argmax  = data.ix[:,data.shape[1]-463:-1].idxmax(axis=1).apply(lambda x: int(str(x).split('_')[-1]))
float(np.sum(argmax==data.label))/data.shape[0]


# ## File accuracy

# In[22]:

reduced = data.groupby('name').median()
argmax  = reduced.ix[:,data.shape[1]-463:-1].idxmax(axis=1).apply(lambda x: int(str(x).split('_')[-1]))
float(np.sum(argmax==reduced.label))/reduced.shape[0]


# In[25]:

import htkmfc
g = data.groupby('name')
data_val = hickle.load(open('../data/TIMIT/timit_id/db/sa/val.hdf5/data.hdf5','r'))
names = np.vectorize(lambda x : x[:-6])(data_val['tag'])

mfcc_path = '../data/TIMIT/timit_id/train/mfcc'

for idx,gr in g:
    mfcc_name = mfcc_path + '/' + idx + '.mfcc'
    mfcc = htkmfc.HTKFeat_read(mfcc_name).getall()
    print idx
    plt.plot(gr.true_class_prob*40)
    plt.imshow(mfcc.T)
    plt.show()

