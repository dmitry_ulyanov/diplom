
# coding: utf-8

# In[3]:

# convert to lower case
get_ipython().system(u'sh to_lower_case')


# In[2]:

# Copies wav files from TIMIT dataset, creates test and train meta data
execfile("prepare_wav.py")


# In[3]:

# Executes mfcc extraction by using Hcopy on files from test.csv and train.csv
execfile("prepare_mfcc.py")


# In[4]:

# Prerares train, val, test sets and stores them in hdf5 files
from subprocess import call
import shlex
#call(shlex.split("python prepare_db.py --train=hdf5 --val=hdf5 --test=hdf5"))
get_ipython().system(u'python prepare_db.py --train=hdf5 --val=hdf5 --test=hdf5')


# In[7]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')

get_ipython().magic(u'run set_up.py')


mean = np.load(timit_folder_path + '/timit_id/db/sa/mean.npy')
plt.imshow(mean.reshape(-1,39))
plt.show()
std = np.load(timit_folder_path + '/timit_id/db/sa/std.npy')
plt.imshow(std.reshape(-1,39))
plt.show()


# In[47]:

def create_binaryproto(array, name, shape):
    blob = caffe_pb2.BlobProto()
    blob.channels, blob.width, blob.height = shape
    blob.num = 1
    blob.data.extend(array.tolist())

    binaryproto_file = open('%s.binaryproto' % name, 'wb')
    binaryproto_file.write(blob.SerializeToString())
    binaryproto_file.close()


# In[49]:

def mean_(mean, binaryproto = True):
    if binaryproto:
    from caffe.proto import caffe_pb2
    from caffe.io import blobproto_to_array

    # Transform a protoblob to a numpy array
    blob = caffe_pb2.BlobProto()
    data = open(mean, "rb").read()
    blob.ParseFromString(data)
    print len(blob.data)
    print blob.num
    nparray = blobproto_to_array(blob)
    else:
    np.load(mean)

    return nparray


# In[16]:

mean_('mean.binaryproto').shape

