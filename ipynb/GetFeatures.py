
# coding: utf-8

# ## Get data from inner layer

# ### LMDB

# In[1]:

import numpy as np
import pandas as pd
caffe_root = '/home/dmitry/caffe_vis/'  
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe
from caffe.proto import caffe_pb2
import os 
import sys 
import lmdb
import datetime
import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')

def classify_from_lmdb(lmdb_path, _deploy, _snapshot, out_layer, batch_size = 100):
    if not os.path.exists(lmdb_path):
        raise Exception('db not found')
 
    lmdb_env = lmdb.open(lmdb_path)  # equivalent to mdb_env_open()
    lmdb_txn = lmdb_env.begin()  # equivalent to mdb_txn_begin()
    lmdb_cursor = lmdb_txn.cursor()  # equivalent to mdb_cursor_open()
    lmdb_cursor.first()  # equivalent to mdb_cursor_get()
    
    nex = True;
    outs = []
    outkeys = []
    outlabels = []
    net = caffe.Classifier(_deploy,_snapshot)
    net.set_phase_test()
    net.set_mode_gpu()
    print 'Starting'
    while nex:
        batch = np.zeros([batch_size,1,40,39])
        keys = []
        labels = []
        a = datetime.datetime.now()
        for i in xrange(batch_size):
            if(nex == False):
                break
            value = lmdb_cursor.value()
            key = lmdb_cursor.key()
    
            datum = caffe_pb2.Datum()
            datum.ParseFromString(value)
            labels.append(datum.label)

            image = np.zeros((datum.channels, datum.height, datum.width))
            image = caffe.io.datum_to_array(datum)
            keys.append (key[:-6])
           
            batch[i,0] = image
            nex = lmdb_cursor.next()
            
        if(nex == False):
            batch = batch[0:i]
            break
    
        #_deploy = '/media/dmitry/A4B231E4B231BB9C/Diplom/model/lenet.prototxt'
        #_snapshot = '/media/dmitry/A4B231E4B231BB9C/Diplom/model/s/ss_iter_50000.caffemodel'
        #print batch[0]
        #net = caffe.Classifier(_deploy,_snapshot)
        #net.set_phase_test()
        #net.set_mode_gpu()
        
        out = net.forward(blobs = [out_layer],**{net.inputs[0]: batch})          
        #out 
        
        
        b = datetime.datetime.now()
        print b-a        
        outlabels.extend(labels)
        # necessary to copy, look at forward_all or just use it
        outs.extend(np.squeeze(out[out_layer].copy())) 
        outkeys.extend(keys);
        print len (outs)
           
        
    lmdb.Environment.close(lmdb_env)
    return (outs,outkeys,outlabels)     


# ### HDF5

# In[8]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')

caffe_root = '/home/dmitry/caffe_vis/'  
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe
from caffe.proto import caffe_pb2

get_ipython().magic(u'run feature_extraction/get_data.py')
get_ipython().magic(u'run feature_extraction/eval.py')

def llfun(pred):
    
    epsilon = 1e-15
    pred = np.maximum(epsilon, pred)
    pred = np.minimum(1-epsilon, pred)
    ll = np.sum(np.log(pred))
    ll = ll * -1.0/len(pred)
    return ll


# ## What to compare

# In[9]:

data_path = '../data/TIMIT/timit_id/db/sa'
exec_dir = '../data/execs/sa_val_data'

models_to_use  = []
what = 'val'
name = 'bug_less2990'
m = {
'name' : name,
'legend' : 'model 1, 40k',
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_40000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)


#name = 'bug_less9326'
m = {
'name' : name,
'legend' : 'model 1, 100k',
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_100000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)


#name = 'bug_less9326'
m = {
'name' : name,
'savedir' : 'q/' + name,
'legend' : 'model 1, 200k',
'exec_dir' : exec_dir,
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_200000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)

#name = 'bug_less9326'
m = {
'name' : name,
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'legend' : 'model 1, 340k',
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_340000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
models_to_use.append(m)


name = 'no_relu_at_the_end_pool_ex_reg720'
m = {
'name' : name,
'savedir' : 'q/' + name,
'legend' : 'model 2, 40k',
'exec_dir' : exec_dir,
'deploy' : exec_dir +'/' + name +  '/configs/deploy.prototxt',
'snapshot' : exec_dir +'/' + name+  '/snapshots/_iter_40000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)


name = 'no_relu_at_the_end_pool_ex_reg720'
m = {
'name' : name,
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'legend' : 'model 2, 200k',
'deploy' : exec_dir +'/' + name +  '/configs/deploy.prototxt',
'snapshot' : exec_dir +'/' + name+  '/snapshots/_iter_200000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)

name = 'no_relu_at_the_end_pool_ex_reg724'
m = {
'name' : name,
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'legend' : 'model 2a, 40k',
'deploy' : exec_dir +'/' + name +  '/configs/deploy.prototxt',
'snapshot' : exec_dir +'/' + name+  '/snapshots/_iter_40000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)



res = []
res1 = []
for m in models_to_use:
    data = get_df(m['test_path'],m['deploy'],m['snapshot'],['prob','ip1']) 
   
    
    data['true_class_prob'] = -1
    unique_labs = data.label.drop_duplicates() 
    for l in unique_labs:
        mask = data.label == l
        data.ix[mask, 'true_class_prob'] = data.ix[mask, 'prob_'+str(l)]
    
    res.append(draw_curves(data))
    
    data.sort (['name','n'],inplace = True)

    res1.append(draw_curves(data))


# In[12]:

get_ipython().magic(u'run feature_extraction/get_data.py')
get_ipython().magic(u'run feature_extraction/eval.py')


res = []
res1 = []
for m in models_to_use:
    data = get_df(m['test_path'],m['deploy'],m['snapshot'],['prob','ip1']) 
   
    
    data['true_class_prob'] = -1
    unique_labs = data.label.drop_duplicates() 
    for l in unique_labs:
        mask = data.label == l
        data.ix[mask, 'true_class_prob'] = data.ix[mask, 'prob_'+str(l)]
    
    res.append(draw_curves(data,'canberra'))
    
    data.sort (['name','n'],inplace = True)

    res1.append(draw_curves(data,'canberra'))

#a = draw_curves(data,'canberra')


# In[6]:

data['data']


# ## Draw on one plot

# In[3]:

plt.figure(figsize=(16,16))
for r in res:
    draw_DET (r[0],r[1])
   
plt.title(what)
plt.legend([x['legend'] for x in models_to_use])
plt.savefig(what + '.pdf')
plt.show()

plt.figure(figsize=(16,16))
for r in res1:
    draw_DET (r[0],r[1])
   
plt.title(what)
plt.legend([x['legend'] for x in models_to_use])
plt.savefig(what + '.pdf')
plt.show()


# In[43]:

draw_curves(data,savedir = m['savedir'])


# In[13]:

exec_dir = '/media/dmitry/A4B231E4B231BB9C/Diplom/data/execs1041/no_relu_at_the_end500'
_deploy = exec_dir + '/configs/deploy.prototxt'
_snapshot =exec_dir + '/snapshots/_iter_100000.caffemodel'
_mean = '/media/dmitry/A4B231E4B231BB9C/TIMIT/timit_id/db/2490/mean.npy'
hdf5_path = '/media/dmitry/A4B231E4B231BB9C/TIMIT/timit_id/db/2490/val.hdf5/data.hdf5'

data = get_df(m['stupid_resize'],m['stupid_resize'],m['stupid_resize'],'ip1', m['mean'])
#data['name'] = np.array([x[:-6] for x in data['tags']])
#data['n'] = np.array([x[-5:] for x in data['tags']])
#data.drop('tags',inplace=True,axis = 0)
data.sort(['name','n'], inplace = True)


# In[53]:

exec_dir = '/media/dmitry/A4B231E4B231BB9C/Diplom/data/execs2490/innovative_more6106'
_deploy = exec_dir + '/configs/deploy.prototxt'
_snapshot =exec_dir + '/snapshots/_iter_345000.caffemodel'
_mean = '/media/dmitry/A4B231E4B231BB9C/TIMIT/timit_id/db/2490/mean.npy'
hdf5_path = '/media/dmitry/A4B231E4B231BB9C/TIMIT/timit_id/db/2490/val.hdf5/data.hdf5'

(out,tags, label) = classify_from_hdf5(hdf5_path,_deploy,_snapshot,'ip1', _mean)

X_pca = PCA().fit_transform(pd.DataFrame(np.vstack(np.squeeze(out))[:,:]).astype('float64'))
# In[65]:

import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')
#data = hickle.load(open(hdf5_path,'r'))
f = plt.figure(figsize=(16,9))
c = ['#ff0000', '#ffff00', '#00ff00', '#00ffff', '#0000ff', 
     '#ff00ff', '#990000', '#999900', '#009900', '#009999']
for i in range(10):
    plt.plot(X_pca[label== i,0], X_pca[label == i,1], '.', c=c[i])
plt.legend(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
plt.grid()
plt.show()


# In[29]:

import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')
f = plt.figure(figsize=(16,9))
c = ['#ff0000', '#ffff00', '#00ff00', '#00ffff', '#0000ff', 
     '#ff00ff', '#990000', '#999900', '#009900', '#009999']
for i in range(10):
    plt.plot(data.ix[data.label == i,2], data.ix[data.label == i,3], '.', c=c[i])
plt.legend(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
plt.grid()
plt.show()


# In[ ]:

data_path = '../data/TIMIT/timit_id/db/sa'
exec_dir = '../data/execs/sa_val_data'

models_to_use  = []
what = 'val'
name = 'bug_less2990'
m = {
'name' : name,
'legend' : 'model 1, 40k',
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_40000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
models_to_use.append(m)


#name = 'bug_less9326'
m = {
'name' : name,
'legend' : 'model 1, 100k',
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_100000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
models_to_use.append(m)


#name = 'bug_less9326'
m = {
'name' : name,
'savedir' : 'q/' + name,
'legend' : 'model 1, 200k',
'exec_dir' : exec_dir,
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_200000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
models_to_use.append(m)

#name = 'bug_less9326'
m = {
'name' : name,
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'legend' : 'model 1, 340k',
'deploy' : exec_dir  +'/' + name + '/configs/deploy.prototxt',
'snapshot' : exec_dir  +'/' + name + '/snapshots/_iter_335000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
models_to_use.append(m)


name = 'no_relu_at_the_end_pool_ex_reg720'
m = {
'name' : name,
'savedir' : 'q/' + name,
'legend' : 'model 2, 40k',
'exec_dir' : exec_dir,
'deploy' : exec_dir +'/' + name +  '/configs/deploy.prototxt',
'snapshot' : exec_dir +'/' + name+  '/snapshots/_iter_40000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)


name = 'no_relu_at_the_end_pool_ex_reg720'
m = {
'name' : name,
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'legend' : 'model 2, 200k',
'deploy' : exec_dir +'/' + name +  '/configs/deploy.prototxt',
'snapshot' : exec_dir +'/' + name+  '/snapshots/_iter_200000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)

name = 'no_relu_at_the_end_pool_ex_reg724'
m = {
'name' : name,
'savedir' : 'q/' + name,
'exec_dir' : exec_dir,
'legend' : 'model 2a, 40k',
'deploy' : exec_dir +'/' + name +  '/configs/deploy.prototxt',
'snapshot' : exec_dir +'/' + name+  '/snapshots/_iter_40000.caffemodel',
'test_path' : data_path + '/' + what + '.hdf5/data.hdf5',
'mean' : data_path + '/mean.npy',
}
#models_to_use.append(m)



res = []
res1 = []
for m in models_to_use:
    data = get_df(m['test_path'],m['deploy'],m['snapshot'],['prob']) 
   
    
    res.append(draw_curves(data))
    
    data.sort (['name','n'],inplace = True)

    res1.append(draw_curves(data))

