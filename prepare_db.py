import pandas as pd

import utils
from data_preparation import create_hdf5
from data_preparation import create_lmdb
import argparse
import random
from set_up import timit_folder_path


parser = argparse.ArgumentParser()
parser.add_argument(
    '--train', default=None, help='comma separated list of types (lmdb, hdf5)')
parser.add_argument(
    '--val', default=None, help='comma separated list of types (lmdb, hdf5)')
parser.add_argument(
    '--test', default=None, help='comma separated list of types (lmdb, hdf5)')

args = parser.parse_args()

timit_new_folder = timit_folder_path + '/timit_id'
wav_config = 'wav_config'
N = 40

# Dirs with MFCC for train and test
path_to_mfcc_train = timit_new_folder + '/train/mfcc'
path_to_mfcc_test = timit_new_folder + '/test/mfcc'


# Split train into train and validation
train = pd.read_csv(timit_new_folder + '/train.csv')
random34 = False
if random34:
    train = utils.shuffle_df(train)

    train_size = int(train.shape[0] * 3 / 4)

    val = train.iloc[train_size:, :]
    train = train.iloc[:train_size, :]
else:
    prefix = train.record_id.apply(lambda x: x[:2])
    val = train.ix[prefix == 'sa', :]
    train = train.ix[prefix != 'sa', :]


if len(train.speaker.unique()) < 462:
    print "bad shuffle, run one again"
    exit()

print "Num speakers in train set: 462"
print "Num speakers in validation set:", len(val.speaker.unique())

# this could be easier with pandas
b = train.speaker.drop_duplicates()
b.reset_index(drop=True, inplace=True)
dict_ = {v: k for k, v in b.to_dict().items()}
train.replace({"speaker": dict_}, inplace=True)
val.replace({"speaker": dict_}, inplace=True)
#

path_to_db = timit_new_folder + '/db/' + str(random.randint(1000, 9999))
utils.mkdirp(path_to_db)

train.to_csv(path_to_db + '/train.csv', index=False)
val.to_csv(path_to_db + '/val.csv', index=False)

# ========= Train ================
if (args.train is not None):
    for use_db in args.train.split(','):
        print "\n- Preparing train (%s)" % use_db

        if use_db == 'lmdb':

            path_to_db_train = path_to_db + '/train.lmdb'

            create_lmdb.create_lmdb(path_to_db_train,
                                    path_to_mfcc_train,
                                    train,
                                    N=N,
                                    compute_mean=False)
        elif use_db == 'hdf5':

            path_to_db_train = path_to_db + '/train.hdf5'

            create_hdf5.create_hdf5(path_to_db_train,
                                    path_to_mfcc_train,
                                    train,
                                    N=N,
                                    compute_mean=True,
                                    substract_mean=True)

        else:
            print 'no such db type'

# ========= Validation ================
if (args.val is not None):
    for use_db in args.val.split(','):
        print "\n- Preparing validation (%s)" % use_db

        if use_db == 'lmdb':

            path_to_db_val = path_to_db + '/val.lmdb'

            create_lmdb.create_lmdb(path_to_db_val,
                                    path_to_mfcc_train,
                                    val,
                                    N=N,
                                    compute_mean=False)
        elif use_db == 'hdf5':

            path_to_db_val = path_to_db + '/val.hdf5'

            create_hdf5.create_hdf5(path_to_db_val,
                                    path_to_mfcc_train,
                                    val,
                                    N=N,
                                    compute_mean=False,
                                    substract_mean=True)

        else:
            print 'no such db type'


# ========= Test ================

test = pd.read_csv(timit_new_folder + '/test.csv')

if (args.test is not None):
    for use_db in args.test.split(','):
        print "\n- Preparing test (%s)" % use_db

        if use_db == 'lmdb':

            path_to_db_test = path_to_db + '/test.lmdb'

            create_lmdb.create_lmdb(path_to_db_test,
                                    path_to_mfcc_test,
                                    test,
                                    N=N,
                                    compute_mean=False)
        elif use_db == 'hdf5':

            path_to_db_test = path_to_db + '/test.hdf5'

            create_hdf5.create_hdf5(path_to_db_test,
                                    path_to_mfcc_test,
                                    test,
                                    N=N,
                                    compute_mean=False,
                                    substract_mean=True)

        else:
            print 'no such db type'
