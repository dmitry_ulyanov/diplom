import os
import csv
from subprocess import call
from set_up import timit_folder_path

# Copies wav files from TIMIT dataset, creates test and train meta data


def mkdirp(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)


timit = timit_folder_path + '/raw'
train_dir = timit + '/train'
test_dir = timit + '/test'

timit_new_folder = timit_folder_path + '/timit_id'
mkdirp(timit_new_folder + '/train/wav')
mkdirp(timit_new_folder + '/test/wav')

# -----------Copy files into one directory, create info file ------------

# Train
info = []
for bdir, _, files in os.walk(train_dir):
    for fname in files:
        if fname.find('.wav') == -1:
            continue

        this_file = os.path.join(bdir, fname)
        ar = bdir.split('/')

        speaker = ar[-1][1:]
        sex = ar[-1][0]
        dialect = ar[-2]

        filename = dialect + '_' + sex + '_' + \
            speaker + '_' + fname[:-4] + '.wav'
        info.append([filename, fname[:-4], speaker, sex, dialect])

        call(['sox', this_file, timit_new_folder + '/train/wav/' + filename])

with open(timit_new_folder + "/train.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerow(['filename', 'record_id', 'speaker', 'sex', 'dialect'])
    writer.writerows(info)

# Test
info = []
for bdir, _, files in os.walk(test_dir):
    for fname in files:
        if fname.find('.wav') == -1:
            continue

        this_file = os.path.join(bdir, fname)
        ar = bdir.split('/')

        speaker = ar[-1][1:]
        sex = ar[-1][0]
        dialect = ar[-2]

        filename = dialect + '_' + sex + '_' + \
            speaker + '_' + fname[:-4] + '.wav'
        info.append([filename, fname[:-4], speaker, sex, dialect])

        call(['sox', this_file, timit_new_folder + '/test/wav/' + filename])

with open(timit_new_folder + "/test.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerow(['filename', 'record_id', 'speaker', 'sex', 'dialect'])
    writer.writerows(info)
