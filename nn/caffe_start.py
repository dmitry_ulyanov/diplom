import sys
import random
import os.path
import datetime
import utils
from google.protobuf.text_format import Merge
from google.protobuf.text_format import PrintMessage
# import gflags
from gflags import DEFINE_string, FLAGS, FlagsError, MarkFlagAsRequired

caffe_root = '/home/dmitry/caffe_vis/'
sys.path.insert(0, caffe_root + 'python')

from caffe.proto import caffe_pb2


DEFINE_string('solver_template_path', None,
              'template to use. It will be edited and stored at exec folder')
DEFINE_string(
    'exec_dir_path', None, 'in this folder directory with all the files will be created')
DEFINE_string('exec_prefix', "", 'exec prefix')
DEFINE_string('train_db_path', None, 'HDF5 db for training')

MarkFlagAsRequired('solver_template_path')
MarkFlagAsRequired('exec_dir_path')
MarkFlagAsRequired('train_db_path')

try:
    argv = FLAGS(sys.argv)  # parse flags
except FlagsError, e:
    print('%s\nUsage: %s ARGS\n%s' % (e, sys.argv[0], FLAGS))
    sys.exit(1)



solver_template_path = FLAGS.solver_template_path
exec_dir_path = FLAGS.exec_dir_path
db_path = FLAGS.train_db_path
exec_prefix = FLAGS.exec_prefix

exec_name = exec_prefix + str(random.randint(0, 9999))

# Get solver info, extract net
solver_ = caffe_pb2.SolverParameter()
with open(solver_template_path, "rb") as f:
    Merge(str(f.read()), solver_)

net_ = caffe_pb2.NetParameter()
with open(solver_.net, "r") as f:
    Merge(str(f.read()), net_)

# Make dirs
configs_dir_path = os.path.join(exec_dir_path, exec_name, 'configs')
snapshots_dir_path = os.path.join(exec_dir_path, exec_name, 'snapshots')

utils.mkdirp(configs_dir_path)
utils.mkdirp(snapshots_dir_path)

solver_path = os.path.join(configs_dir_path, 'solver.prototxt')
train_test_path = os.path.join(configs_dir_path, 'train_test.prototxt')

# Change configs
solver_.net = train_test_path
solver_.snapshot_prefix = snapshots_dir_path + '/'

# Copy configs
with open(solver_path, 'w') as f:
    PrintMessage(solver_, f)

with open(train_test_path, 'w') as f:
    PrintMessage(net_, f)
# shutil.copy(solver_template_path, configs_dir_path)
# shutil.copy(solver_.net, configs_dir_path)

# ========================================================================

# Get batch_size and decide how many objects to use
batch_size = net_.layers[0].memory_data_param.batch_size

# Prepare cmd command
agrs = '--solver_path=%s \\\n--log_file=%s \\\n--batch_size=%d \\\n--db_path=%s' % (
    solver_path, os.path.join(exec_dir_path, exec_name, 'log.log'), batch_size, db_path)
tee_part = ' 2>&1 | tee %s' % os.path.join(exec_dir_path, exec_name, 'log.log')
cmd_string = "python nn/caffe_train.py " + agrs  # + tee_part


# Viz server command
import socket


def PickUnusedPort():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    addr, port = s.getsockname()
    s.close()
    return port
# port = PickUnusedPort()
# print port
port = 8000

viz_server_command = 'python %spython/viz_server.py \\\n--port=%d \\\n--log_directory=%s \\\n--static_directory=%sstatic &' % (
    caffe_root, port, os.path.join(exec_dir_path, exec_name), caffe_root)

# file with args and commands
misc_path = os.path.join(exec_dir_path, exec_name, 'misc')
with open(misc_path, 'w') as f:
    f.write('%s\n' % str(datetime.datetime.now()))
    f.write('\npython %s\n' % ' '.join(sys.argv))
    f.write('\n%s\n' % cmd_string)
    f.write('\n%s\n' % viz_server_command)


from subprocess import Popen
import shlex
Popen(shlex.split('python %spython/viz_server.py --port=%d --log_directory=%s --static_directory=%s/static &' %
                  (caffe_root, port, os.path.join(exec_dir_path, exec_name), caffe_root)))


# can't get output to log working
# from subprocess import call
# import shlex
# call(shlex.split(cmd_string))
# Popen(shlex.split(cmd_string))

os.system(cmd_string)
