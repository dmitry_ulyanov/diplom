import sys
import hickle
import numpy as np
from gflags import DEFINE_string, FLAGS, FlagsError, DEFINE_integer, MarkFlagAsRequired

caffe_root = '/home/dmitry/caffe_vis/'
sys.path.insert(0, caffe_root + 'python')

import caffe

DEFINE_string('solver_path', None, 'solver')
DEFINE_string('log_file', None, 'LOG FILE')
DEFINE_string('db_path', None, 'HDF5 db to feed')

DEFINE_integer('batch_size', None, 'batch size')


MarkFlagAsRequired('solver_path')
MarkFlagAsRequired('log_file')
MarkFlagAsRequired('batch_size')
MarkFlagAsRequired('db_path')


try:
    argv = FLAGS(sys.argv)  # parse flags
except FlagsError, e:
    print('%s\nUsage: %s ARGS\n%s' % (e, sys.argv[0], FLAGS))
    sys.exit(1)

solver_path = FLAGS.solver_path
db_path = FLAGS.db_path
batch_size = FLAGS.batch_size
log_file = FLAGS.log_file


data = hickle.load(open(db_path, 'r'))

num_obj_to_use = data['data'].shape[0] - (data['data'].shape[0] % batch_size)
print num_obj_to_use

solver = caffe.SGDSolver(solver_path)
solver.net.set_mode_gpu()
solver.add_visualizers(log_file)

solver.net.set_input_arrays(data['data'][:num_obj_to_use, :,:,:],
                            data['label'].astype(np.float32)[:num_obj_to_use])
solver.solve()
