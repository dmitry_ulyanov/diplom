import os
import pandas as pd
from subprocess import call
import utils
from set_up import timit_folder_path
# Executes mfcc extraction by using Hcopy on files from test.csv and train.csv

# ------ Create mfcc ------------------------


def mkdirp(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)


timit_new_folder = timit_folder_path + '/timit_id'
wav_config = 'wav_config'

timit_wav_train = timit_new_folder + '/train/wav'
timit_wav_test = timit_new_folder + '/test/wav'

mkdirp(timit_new_folder + '/train/mfcc')
mkdirp(timit_new_folder + '/test/mfcc')

# Train
print 'Train'
train = pd.read_csv(timit_new_folder + '/train.csv')
for index, row in train.iterrows():
    fname = row.filename

    this_file = os.path.join(timit_wav_train, fname)
    mfcc_file = timit_new_folder + "/train/mfcc/" + fname[:-4] + ".mfcc"

    call(['HCopy', '-C', wav_config, this_file, mfcc_file])

    utils.update_progress((index + 1) / float(train.shape[0]))
# Test
print 'Test'
test = pd.read_csv(timit_new_folder + '/test.csv')
for index, row in test.iterrows():
    fname = row.filename

    this_file = os.path.join(timit_wav_test, fname)
    mfcc_file = timit_new_folder + "/test/mfcc/" + fname[:-4] + ".mfcc"

    call(['HCopy', '-C', wav_config, this_file, mfcc_file])

    utils.update_progress((index + 1) / float(test.shape[0]))
