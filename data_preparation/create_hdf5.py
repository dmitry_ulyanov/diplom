import htkmfc
import hickle
import numpy as np
import utils


def create_hdf5(path_to_db,
                path_to_mfcc,
                df,
                shuffle_idx=True,
                compute_mean=True,
                substract_mean=True,
                div_by_std=False,
                N=40):

    # Create flolder and db
    utils.del_and_create(path_to_db)
    tags = []
    objects = []
    labels = []

    for index, (ix, row) in enumerate(df.iterrows()):
        fname = row.filename
        utils.update_progress(index / float(df.shape[0]))
        mfcc_file = path_to_mfcc + '/' + fname[:-4] + '.mfcc'
        mfcc = htkmfc.HTKFeat_read(mfcc_file).getall()

        n = 1

        for i in xrange(0, mfcc.shape[0] - N - 1):
            m = mfcc[i:i + N]

            objects.append(m[None,:,:])
            tags.append('%s_%05d' % (fname[:-4], n))
            n += 1

        labels.extend([row.speaker for i in xrange(0, mfcc.shape[0] - N - 1)])

    print '\n Total number of objects ', len(objects)
    shuffle_ind = range(len(objects))
    np.random.shuffle(shuffle_ind)

    data = np.array(objects)[shuffle_ind,:,:,:]
    labels = np.array(labels)[shuffle_ind]
    tags = np.array(tags)[shuffle_ind]

    del objects

    if substract_mean:
        if compute_mean:
            mean = np.mean(data, axis=0)
            std = np.std(data, axis=0)
        else:
            mean = np.load(path_to_db + '/../mean.npy')
            std = np.load(path_to_db + '/../std.npy')

        for i in range(data.shape[0]):
            data[i] -= mean
            if div_by_std:
                data[i] /= (std + 10e-7)

        print np.mean(data, axis=0)

        np.save(path_to_db + '/../mean', mean)
        np.save(path_to_db + '/../std', std)
        utils.create_binaryproto(
            np.squeeze(mean), path_to_db + '/../mean', (1, N, 39))
        utils.create_binaryproto(
            np.squeeze(std), path_to_db + '/../std', (1, N, 39))

    hickle.dump({'data': data,
                 'label': labels,
                 'tag': tags},
                path_to_db + '/data.hdf5', 'w', compression='gzip')
    # if compute_mean:
        # for i, file_num_obj in enumerate(lens):
            #file_means[i] *= float(file_num_obj) / float(obj_num)
            #file_squares[i] *= float(file_num_obj) / float(obj_num)
#
        #global_mean = np.sum(file_means, axis=0)
        #global_mean_squares = np.sum(file_squares, axis=0)
        # var = obj_num / (obj_num - 1) * \
            #(global_mean_squares - np.power(global_mean, 2))
        #np.save('mean', global_mean)
        #np.save('var', var)
        #utils.create_binaryproto(global_mean, 'mean', (1, N, 39))
        #utils.create_binaryproto(var, 'var', (1, N, 39))
