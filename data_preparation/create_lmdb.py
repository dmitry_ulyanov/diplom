import random
import sys

import htkmfc
import lmdb
import numpy as np

caffe_root = '/home/dmitry/caffe_vis/'
sys.path.insert(0, caffe_root + 'python')
from caffe.proto import caffe_pb2

import utils


def create_lmdb(path_to_db, path_to_mfcc, df,
                shuffle_idx=True, compute_mean=True, N=40):

    # Create floder and db
    utils.del_and_create(path_to_db)
    img_env = lmdb.Environment(path_to_db, map_size=1099511627776)
    img_txn = img_env.begin(write=True, buffers=True)

    file_means = []
    file_squares = []
    lens = []

    for index, (ix, row) in enumerate(df.iterrows()):
        fname = row.filename
        utils.update_progress(index / float(df.shape[0]))

        mfcc_file = path_to_mfcc + "/" + fname[:-4] + ".mfcc"

        mfcc = htkmfc.HTKFeat_read(mfcc_file).getall()

        n = 1
        mean = np.zeros(39 * N)
        squares = np.zeros(39 * N)

        for i in xrange(0, mfcc.shape[0] - N - 1):
            m = mfcc[i:i + N, :]

            datum = caffe_pb2.Datum()
            datum.channels = 1
            datum.height = m.shape[0]
            datum.width = m.shape[1]

            # flatten row major as caffe uses
            m = np.hstack(m)

            if compute_mean:
                mean += m
                squares += np.power(m, 2)

            datum.float_data.extend(m.tolist())
            datum.label = row.speaker
            s = datum.SerializeToString()

            if shuffle_idx:
                key = '00000000_%d_%05d' % (random.randint(0, sys.maxint), n)
            else:
                key = '00000000_%s_%05d' % (fname[:-4], n)

            img_txn.put(key, s)
            n += 1

        lens.append(n)
        if compute_mean:
            file_means.append(mean / n)
            file_squares.append(squares / n)

    obj_num = np.sum(lens)
    print '\n Total number of objects ', obj_num

    if compute_mean:

        for i, file_num_obj in enumerate(lens):
            file_means[i] *= (float(file_num_obj) / float(obj_num))
            file_squares[i] *= (float(file_num_obj) / float(obj_num))

        global_mean = np.sum(file_means, axis=0)
        global_mean_squares = np.sum(file_squares, axis=0)

        var = obj_num / (obj_num - 1) * \
            (global_mean_squares - np.power(global_mean, 2))

        np.save('mean', global_mean)
        np.save('var', var)

        # Use the last datum to determine the shape
        utils.create_binaryproto(
            global_mean, 'mean', (datum.channels,   datum.width, datum.height))
        utils.create_binaryproto(
            var, 'var', (datum.channels,   datum.width, datum.height))

    img_txn.commit()
    img_env.close()
