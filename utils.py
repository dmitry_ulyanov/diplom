import os
import random
import shutil
import sys


caffe_root = '/home/dmitry/caffe_vis/'
sys.path.insert(0, caffe_root + 'python')
from caffe.proto import caffe_pb2


def shuffle_df(df):
    idx = df.index.values.tolist()
    random.shuffle(idx)
    df = df.ix[idx]  # copy

    df.reset_index(inplace=True, drop=True)
    return df


def mkdirp(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)


def del_and_create(dname):
    if os.path.exists(dname):
        shutil.rmtree(dname)
        #os.system("rm -rf %s" % dname)

    os.makedirs(dname)


def update_progress(progress):
    print "\rProgress: [{0:50s}] {1:.1f}%".format('#' * int(progress * 50),
                                                  progress * 100),


def create_binaryproto(array, name, shape):
    blob = caffe_pb2.BlobProto()
    blob.channels, blob.height, blob.width = shape
    blob.num = 1

    blob.data.extend(array.flatten().tolist())

    binaryproto_file = open('%s.binaryproto' % name, 'wb')
    binaryproto_file.write(blob.SerializeToString())
    binaryproto_file.close()
